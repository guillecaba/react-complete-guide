import React, { Component, Fragment } from "react";
import classes from "./Person.css";
import Aux from "../../../hoc/Auxiliary";
import withClass from "../../../hoc/withClass";
import PropTypes from "prop-types";
import AuthContext from "../../../context/auth-context";

class Person extends Component {
  constructor(props) {
    super(props);
    this.inputElementRef = React.createRef();
  }

  static contextType = AuthContext;
  componentDidMount() {
    this.inputElementRef.current.focus();
    console.log(this.context.authenticated);
  }
  render() {
    console.log("[Person.js] rendering...");
    return (
      <Fragment>
        {this.context.authenticated ? (
          <p>Autenticado!</p>
        ) : (
          <p>Por favor logea</p>
        )}

        <p key="i1" onClick={this.props.click}>
          {" "}
          Soy {this.props.name} y tengo {this.props.age}
        </p>
        <p key="i2"> {this.props.children}</p>
        <input
          key="i3"
          /*ref = {(inputEl)=> { this.inputElement = inputEl }} */ ref={
            this.inputElementRef
          }
          type="text"
          onChange={this.props.changed}
          value={this.props.name}
        />
      </Fragment>
    );
  }
}

Person.propTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func
};

export default withClass(Person, classes.Person);
