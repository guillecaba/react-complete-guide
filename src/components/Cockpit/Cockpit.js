import React, { useEffect, useRef, useContext } from "react";
import classes from "./Cockpit.css";

import AuthContext from "../../context/auth-context";

const cockpit = props => {
  const toogleBtnRef = useRef(null);
  const authContext = useContext(AuthContext);

  useEffect(() => {
    console.log("[Cockpit.js] useEffect");

    /* setTimeout( ()=> {
        alert('Saved data in the cloud');
      },1000); */
    toogleBtnRef.current.click();
    return () => {
      console.log("[Cockpit.js] cleanup work in useEffect ");
    };
  }, []);

  useEffect(() => {
    console.log("[Cockpit.js] useEffect 2");

    return () => {
      console.log("[Cockpit.js] cleanup work in 2nd useEffect");
    };
  });

  const assignedClasses = [];
  let btnClass = "";
  if (props.showPersons) {
    btnClass = classes.Red;
  }

  if (props.personsLength <= 2) {
    assignedClasses.push(classes.red); // classes = ['red]
  }
  if (props.personsLength <= 1) {
    assignedClasses.push(classes.bold); // classes = ['red','bold']
  }
  return (
    <div className={classes.Cockpit}>
      <h1>Hi, I'm a React App</h1>
      <p className={assignedClasses.join(" ")}>This is really working!</p>
      <button ref={toogleBtnRef} className={btnClass} onClick={props.clicked}>
        Toogle Persons
      </button>

      <button onClick={authContext.login}>Log In</button>
    </div>
  );
};

export default React.memo(cockpit);
